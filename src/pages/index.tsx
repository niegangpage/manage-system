import styles from './index.less';
import React, {
  useState,
  useReducer,
  FC,
  ReactElement,
  useRef,
  useCallback,
} from 'react';
import { API } from '../utils/request';
import { Button } from 'antd';
import Child from '../components/child';

interface IncrementAC {
  type: 'increment';
  payload: number;
}
//reducer的action约束;
interface DecrementAC {
  type: 'decrement';
  payload: number;
}
//reducer的action约束;
type Action = IncrementAC | DecrementAC;
////合并reducer的action约束;

interface IArr {
  item: {
    name: string;
    age: number;
    note?: string;
  }[];
}
//规定一个数组中的对象元素结构;

interface IProps {
  people: {
    name: string;
    age: number;
    url: string;
    note?: string | undefined;
    //可选
  }[];
}
//规定一个数组中的对象元素结构;

const reducer = (state: number, action: Action) => {
  switch (action.type) {
    case 'increment':
      return state + action.payload;
    case 'decrement':
      return state + action.payload;
    default:
      return state;
  }
};
//reducer内部的数据被Action进行约束;

const IndexPage = (): ReactElement => {
  const [num, dispatch] = useReducer(reducer, 0);
  const [arr, setArr] = useState<IArr['item']>([{ name: '张三', age: 9 }]);
  //useState内部的数据被IArr约束;

  const [people, setPeople] = useState<IProps['people']>([
    {
      name: '张三',
      age: 10,
      url: 'www.baidu.com',
      note: '你好',
    },
  ]);
  //useState内部的数据被IProps约束;

  const login = async () => {
    const res = await API.post('/login');
    console.log('登陆结果', res);
    if (res) {
      const { token } = res.data;
      localStorage.setItem('token', token);
    }
  };
  //数据请求-登陆;

  const getMenu = async (): Promise<any> => {
    const res = await API.get('/menu');
    console.log('菜单', res);
  };
  //数据请求-菜单;

  const addItem = (): void => {
    setArr([...arr, { name: '周新', age: 10 }]);
  };
  //扩展更新地址,检测后数据修改Render;

  const increment = (parems: number = 0): void => {
    dispatch({
      type: 'increment',
      payload: parems,
    });
  };
  //派发action修改数据;

  const decrement = (parems: number = 0): void => {
    dispatch({
      type: 'decrement',
      payload: -1,
    });
  };
  //派发action修改数据;

  //表单处理;
  const inputRef = useRef<HTMLInputElement>(null);
  const handleChange = (): void => {
    console.log(inputRef.current?.value);
    setCount(inputRef.current?.value);
  };
  //表单处理;

  const selectRef = useRef<HTMLSelectElement>(null);
  const selectChange = (): void => {
    console.log(selectRef.current?.value);
    setCount(inputRef.current?.value);
  };

  //函数形式传值;useCallback;
  const [count, setCount] = useState<number | string | undefined>(10);
  const callback = useCallback(() => {
    return count;
  }, [count]);
  const setParentCount = () => {
    setCount(100);
    //修改ParentCount;
  };
  //函数形式传值;useCallback;

  //传入一个带返回值的函数;
  const show = () => {
    return 1000;
  };
  //传入一个带返回值的函数;

  //常规传值;
  const [name, setName] = useState<string>('默认值');
  const setParentName = () => {
    setName('修改值');
  };
  //常规传值;

  //常规传值;
  const [gender, setGender] = useState<string>('女');
  const setParentGender = () => {
    setGender('男');
  };
  //常规传值;

  return (
    <div>
      {/* 直接传值 */}
      <h1 className={styles.title}>父组件的name{name}</h1>
      <button onClick={() => setParentName()}>修改的父组件的name</button>
      {/* 直接传值 */}

      {/* 直接传值 */}
      <h1 className={styles.title}>父组件的性别{gender}</h1>
      <button onClick={() => setParentGender()}>修改的父组件的性别</button>
      {/* 直接传值 */}

      <h1 className={styles.title}>父组件的count{count}</h1>
      <button onClick={() => setParentCount()}>修改的父组件的count</button>
      <Child
        people={people}
        increment={increment}
        callback={callback}
        name={name}
        gender={gender}
        show={show}
      />
      <h1>{num}</h1>
      {arr.map((item, index) => {
        return <div key={index}>{item.name}</div>;
      })}
      <Button onClick={() => addItem()} type="primary">
        Button
      </Button>
      <button onClick={() => increment(1)}>+</button>
      <button onClick={() => decrement()}>-</button>
      <button className={styles.btn} onClick={() => login()}>
        登陆
      </button>
      <button className={styles.btn} onClick={() => getMenu()}>
        菜单
      </button>
      <input
        type="text"
        ref={inputRef}
        placeholder="请输入内容"
        onChange={() => handleChange()}
      />
      <select ref={selectRef} onChange={() => selectChange()} name="选项" id="">
        <option value="成都被选择">成都</option>
        <option value="北京被选择">北京</option>
        <option value="上海被选择">上海</option>
      </select>
    </div>
  );
};
export default IndexPage;
