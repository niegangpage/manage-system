import React, { FC, ReactElement, useState, useEffect } from 'react';
interface IProps {
  increment: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent> | any,
  ) => void;
  show: any;
  callback: any;
  name: string;
  gender: string;
  people: {
    name: string;
    age: number;
    url: string;
    note?: string | undefined;
  }[];
}
//约束IProps传入数据;

const child: React.FC<IProps> = (props): ReactElement => {
  const { increment, people, callback, name, gender, show } = props;
  //解构Props;
  // console.log("父组件传入的name数据",name);
  // console.log("父组件传入的name数据",gender);
  // console.log("赋组件传入的callback函数",callback);

  //useCallback
  const [count, setCount] = useState(() => callback());
  //callback()调用就是返回一个count数据,然后交给来useState;此时的count放到页面进行渲染;
  useEffect(() => {
    setCount(callback());
  }, [callback]);
  //监听父组件传入的callback函数的count依赖是否发生变化,只有当count变化时才进行赋值,给予子组件进行渲染;
  //如果我不去监听这个callback函数,它就会脱离父组件的传值依赖渲染,只有当我监听这个callback函数的依赖变化时,才会保持与父组件的同步渲染;
  //useCallback

  //接收带返回值的函数并调用赋值;
  useEffect(() => {
    setCount(show());
  }, []);
  //接收带返回值的函数并调用赋值;

  //调用父组件的函数并传值;
  const get = () => {
    increment(10);
  };
  //调用父组件的函数并传值;

  return (
    <div>
      <h1>{count}</h1>
      <h1>子组件的性别{gender}</h1>
      <h1>子组件的name{name}</h1>
      <h1>子组件的count{count}</h1>
      {people.map((item: any, index: number) => {
        return (
          <div key={index}>
            {item.name}
            {item.age}
          </div>
        );
      })}
      <button onClick={() => increment(2)}>父组件传入的事件</button>
      <button onClick={() => get()}>子组件的按钮</button>
      <h1>你好</h1>
    </div>
  );
};
export default child;
