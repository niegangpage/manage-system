import axios from 'axios';
// 请求拦截器
axios.interceptors.request.use(
  (config: any) => {
    // 拦截请求 携带token
    const token = localStorage.token;
    config.headers.Authorization = token;
    //拦截请求headers加上Token;
    //后端接口写错;
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);
// 请求拦截器

// 响应拦截器
axios.interceptors.response.use(
  (res) => {
    return res.data;
  },
  (error) => {
    if (error && error.response && error.response.status) {
      switch (error.response.status) {
        case 401:
          console.log(401);
          break;
        case 404:
          console.log(404);
          break;
        default:
          console.log(200);
          break;
      }
    } else {
    }
  },
);
// 响应拦截器

//基础API路径;
axios.defaults.baseURL = 'http://xawn.f3322.net:8012';
//基础API路径;

//封装接口;
interface IState {
  post: any;
  get: any;
}
//封装接口;

//API调用对象;
export const API: IState = {
  post: '',
  get: '',
};
//API调用对象;

//POST请求;
API.post = function (api: string, data: any, params: any): any {
  return new Promise((resolve, reject) => {
    axios({
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      url: api,
      data: `username=mrbird&password=1234qwer`,
    }).then((res) => {
      resolve(res);
    });
  });
};
//POST请求;

//GET请求;
API.get = function (api: string, data: any, params: any) {
  return new Promise((resolve, reject) => {
    axios({
      method: 'GET',
      url: `${api}/mrbird`,
    }).then((res) => {
      resolve(res);
    });
  });
};
//GET请求;
